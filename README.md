# lox_cli

This is an in-development client library and CLI for Lox, designed to talk to the Lox Distributor.

## Server

The Lox Distributor code can be found [here](https://gitlab.torproject.org/tpo/anti-censorship/lox/-/tree/main/crates/lox-distributor). I am aiming to keep this project up-to-date to work with that repository. For testing, use [this fork](https://gitlab.torproject.org/vecna/lox/-/tree/main/crates/lox-distributor) which adds an endpoint to the server to allow artificially increasing the number of days that have passed.

## Usage

Make sure the server is running first.

`cargo run` gets a new open invite Lox credential.

`cargo run -- -L` performs a trust migration (level 0) or level up (level 1+) if possible.

`cargo run -- -I` issues an invitation if possible.

`cargo run -- -R invite_file` redeems an invitation to get a level 1 Lox credential.

`cargo run -- -h` for more options.
