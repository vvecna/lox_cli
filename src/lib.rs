use anyhow::{anyhow, Result};
use lox_library::{
    bridge_table::{from_scalar, BridgeLine, BridgeTable, EncryptedBucket, MAX_BRIDGES_PER_BUCKET},
    cred,
    proto::{
        level_up::{LEVEL_INTERVAL, MAX_BLOCKAGES},
        *,
    },
    scalar_u32, IssuerPubKey, OPENINV_LENGTH,
};
use lox_utils::{EncBridgeTable, Invite};
use std::collections::HashMap;

pub mod networking;
use crate::networking::Networking;

// Helper functions to get public keys from vector
pub fn get_lox_pub(lox_auth_pubkeys: &Vec<IssuerPubKey>) -> &IssuerPubKey {
    &lox_auth_pubkeys[0]
}

pub fn get_migration_pub(lox_auth_pubkeys: &Vec<IssuerPubKey>) -> &IssuerPubKey {
    &lox_auth_pubkeys[1]
}

pub fn get_migrationkey_pub(lox_auth_pubkeys: &Vec<IssuerPubKey>) -> &IssuerPubKey {
    &lox_auth_pubkeys[2]
}

pub fn get_reachability_pub(lox_auth_pubkeys: &Vec<IssuerPubKey>) -> &IssuerPubKey {
    &lox_auth_pubkeys[3]
}

pub fn get_invitation_pub(lox_auth_pubkeys: &Vec<IssuerPubKey>) -> &IssuerPubKey {
    &lox_auth_pubkeys[4]
}

// Helper function to check if credential is eligible for
// promotion from level 0 to 1
pub async fn eligible_for_trust_promotion(
    net: &dyn Networking,
    cred: &lox_library::cred::Lox,
) -> bool {
    let level_since: u32 = match scalar_u32(&cred.level_since) {
        Some(v) => v,
        None => return false,
    };
    let date = match get_today(net).await {
        Ok(v) => v,
        Err(e) => {
            eprintln!("Failed to get date, error: {}", e);
            return false;
        }
    };
    match scalar_u32(&cred.trust_level) {
        Some(v) => v == 0 && level_since + trust_promotion::UNTRUSTED_INTERVAL <= date,
        None => false,
    }
}

// Helper function to check if credential is eligible for
// level up from level 1+
pub async fn eligible_for_level_up(net: &dyn Networking, cred: &lox_library::cred::Lox) -> bool {
    let level_since: u32 = match scalar_u32(&cred.level_since) {
        Some(v) => v,
        None => return false,
    };
    let date = match get_today(net).await {
        Ok(v) => v,
        Err(e) => {
            eprintln!("Failed to get date, error: {}", e);
            return false;
        }
    };
    let trust_level = match scalar_u32(&cred.trust_level) {
        Some(v) => v,
        None => return false,
    };
    let blockages = match scalar_u32(&cred.blockages) {
        Some(v) => v,
        None => return false,
    };
    trust_level > 0
        && blockages <= MAX_BLOCKAGES[trust_level as usize]
        && level_since + LEVEL_INTERVAL[trust_level as usize] <= date
}

// Get current date from Lox Auth
pub async fn get_today(net: &dyn Networking) -> Result<u32> {
    let resp = net.request("/today".to_string(), [].to_vec()).await?;
    let today: u32 = serde_json::from_slice(&resp)?;
    Ok(today)
}

// Download Lox Auth pubkeys
pub async fn get_lox_auth_keys(net: &dyn Networking) -> Result<Vec<IssuerPubKey>> {
    let resp = net.request("/pubkeys".to_string(), [].to_vec()).await?;
    let lox_auth_pubkeys: Vec<IssuerPubKey> = serde_json::from_slice(&resp)?;
    Ok(lox_auth_pubkeys)
}

// Get encrypted bridge table
pub async fn get_reachability_credential(
    net: &dyn Networking,
) -> Result<HashMap<u32, EncryptedBucket>> {
    let resp = net
        .request("/reachability".to_string(), [].to_vec())
        .await?;
    let reachability_cred: EncBridgeTable = serde_json::from_slice(&resp)?;
    Ok(reachability_cred.etable)
}

// Get encrypted bridge table from BridgeDB and decrypt our entry
pub async fn get_bucket(
    net: &dyn Networking,
    lox_cred: &lox_library::cred::Lox,
) -> Result<(
    [BridgeLine; MAX_BRIDGES_PER_BUCKET],
    Option<cred::BucketReachability>,
)> {
    let encbuckets = get_reachability_credential(net).await?;
    let (id, key) = match from_scalar(lox_cred.bucket) {
        Ok((id, key)) => (id, key),
        Err(e) => {
            return Err(anyhow!(
                "aead error returned when trying to get id and key from bucket: {}",
                e
            ))
        }
    };
    let encbucket = match encbuckets.get(&id) {
        Some(v) => v,
        None => {
            return Err(anyhow!(
                "Unable to get encrypted bucket from encrypted bridge table"
            ))
        }
    };
    match BridgeTable::decrypt_bucket(id, &key, &encbucket) {
        Ok(v) => Ok(v),
        Err(e) => Err(anyhow!(
            "aead error returned when trying to decrypt bucket: {}",
            e
        )),
    }
}

// Get an open invitation
pub async fn get_open_invitation(net: &dyn Networking) -> Result<[u8; OPENINV_LENGTH]> {
    let resp = net.request("/invite".to_string(), [].to_vec()).await?;
    let open_invite: [u8; OPENINV_LENGTH] = serde_json::from_slice::<Invite>(&resp)?.invite;
    Ok(open_invite)
}

// Get a Lox Credential from an open invitation
pub async fn get_lox_credential(
    net: &dyn Networking,
    open_invite: &[u8; OPENINV_LENGTH],
    lox_pub: &IssuerPubKey,
) -> Result<(lox_library::cred::Lox, BridgeLine)> {
    let (req, state) = open_invite::request(&open_invite);
    let encoded_req: Vec<u8> = serde_json::to_vec(&req)?;
    let encoded_resp = net.request("/openreq".to_string(), encoded_req).await?;
    let decoded_resp: open_invite::Response = serde_json::from_slice(&encoded_resp)?;
    let (cred, bridgeline) = open_invite::handle_response(state, decoded_resp, &lox_pub)?;
    Ok((cred, bridgeline))
}

// Get a migration credential to migrate to higher trust
pub async fn trust_promotion(
    net: &dyn Networking,
    lox_cred: &lox_library::cred::Lox,
    lox_pub: &IssuerPubKey,
) -> Result<lox_library::cred::Migration> {
    let (req, state) = trust_promotion::request(&lox_cred, &lox_pub, get_today(net).await?)?;
    let encoded_req: Vec<u8> = serde_json::to_vec(&req)?;
    let encoded_resp = net.request("/trustpromo".to_string(), encoded_req).await?;
    let decoded_resp: trust_promotion::Response = serde_json::from_slice(&encoded_resp)?;
    let migration_cred = trust_promotion::handle_response(state, decoded_resp)?;
    Ok(migration_cred)
}

// Promote from untrusted (trust level 0) to trusted (trust level 1)
pub async fn trust_migration(
    net: &dyn Networking,
    lox_cred: &lox_library::cred::Lox,
    migration_cred: &lox_library::cred::Migration,
    lox_pub: &IssuerPubKey,
    migration_pub: &IssuerPubKey,
) -> Result<lox_library::cred::Lox> {
    let (req, state) = migration::request(lox_cred, migration_cred, lox_pub, migration_pub)?;
    let encoded_req: Vec<u8> = serde_json::to_vec(&req)?;
    let encoded_resp = net.request("/trustmig".to_string(), encoded_req).await?;
    let decoded_resp: migration::Response = serde_json::from_slice(&encoded_resp)?;
    let cred = migration::handle_response(state, decoded_resp, lox_pub)?;
    Ok(cred)
}

// Increase trust from at least level 1 to higher levels
pub async fn level_up(
    net: &dyn Networking,
    lox_cred: &lox_library::cred::Lox,
    reachcred: &cred::BucketReachability,
    lox_pub: &IssuerPubKey,
    reachability_pub: &IssuerPubKey,
) -> Result<lox_library::cred::Lox> {
    let (req, state) = level_up::request(
        lox_cred,
        &reachcred,
        lox_pub,
        reachability_pub,
        get_today(net).await?,
    )?;
    let encoded_req: Vec<u8> = serde_json::to_vec(&req)?;
    let encoded_resp = net.request("/levelup".to_string(), encoded_req).await?;
    let decoded_resp: level_up::Response = serde_json::from_slice(&encoded_resp)?;
    let cred = level_up::handle_response(state, decoded_resp, lox_pub)?;
    Ok(cred)
}

// Request an Invitation credential to give to a friend
pub async fn issue_invite(
    net: &dyn Networking,
    lox_cred: &lox_library::cred::Lox,
    encbuckets: &HashMap<u32, EncryptedBucket>,
    lox_pub: &IssuerPubKey,
    reachability_pub: &IssuerPubKey,
    invitation_pub: &IssuerPubKey,
) -> Result<(lox_library::cred::Lox, lox_library::cred::Invitation)> {
    // Read the bucket in the credential to get today's Bucket
    // Reachability credential

    let (id, key) = match from_scalar(lox_cred.bucket) {
        Ok((id, key)) => (id, key),
        Err(e) => {
            return Err(anyhow!(
                "aead error returned when trying to get id and key from bucket: {}",
                e
            ))
        }
    };
    let encbucket = match encbuckets.get(&id) {
        Some(v) => v,
        None => {
            return Err(anyhow!(
                "Unable to get encrypted bucket from encrypted bridge table"
            ))
        }
    };
    let bucket = match BridgeTable::decrypt_bucket(id, &key, encbucket) {
        Ok(v) => v,
        Err(e) => {
            return Err(anyhow!(
                "aead error returned when trying to decrypt bucket: {}",
                e
            ))
        }
    };
    let reachcred = match bucket.1 {
        Some(v) => v,
        None => {
            return Err(anyhow!(
                "Expected reachability credential but none was found"
            ))
        }
    };

    let (req, state) = issue_invite::request(
        lox_cred,
        &reachcred,
        lox_pub,
        reachability_pub,
        get_today(net).await?,
    )?;
    let encoded_req: Vec<u8> = serde_json::to_vec(&req)?;
    let encoded_resp = net.request("/issueinvite".to_string(), encoded_req).await?;
    let decoded_resp: issue_invite::Response = serde_json::from_slice(&encoded_resp)?;
    let (cred, invite) =
        issue_invite::handle_response(state, decoded_resp, lox_pub, invitation_pub)?;
    Ok((cred, invite))
}

// Redeem an Invitation credential to start at trust level 1
pub async fn redeem_invite(
    net: &dyn Networking,
    invite: &lox_library::cred::Invitation,
    lox_pub: &IssuerPubKey,
    invitation_pub: &IssuerPubKey,
) -> Result<(lox_library::cred::Lox, [BridgeLine; MAX_BRIDGES_PER_BUCKET])> {
    let (req, state) = redeem_invite::request(invite, invitation_pub, get_today(net).await?)?;
    let encoded_req: Vec<u8> = serde_json::to_vec(&req)?;
    let encoded_resp = net.request("/redeem".to_string(), encoded_req).await?;
    let decoded_resp: redeem_invite::Response = serde_json::from_slice(&encoded_resp)?;
    let cred = redeem_invite::handle_response(state, decoded_resp, lox_pub)?;

    let bucket = get_bucket(net, &cred).await?.0;
    Ok((cred, bucket))
}

// Check for a migration credential to move to a new bucket
pub async fn check_blockage(
    net: &dyn Networking,
    lox_cred: &lox_library::cred::Lox,
    lox_pub: &IssuerPubKey,
) -> Result<lox_library::cred::Migration> {
    let (req, state) = check_blockage::request(lox_cred, lox_pub)?;
    let encoded_req: Vec<u8> = serde_json::to_vec(&req)?;
    let encoded_resp = net
        .request("/checkblockage".to_string(), encoded_req)
        .await?;
    let decoded_resp: check_blockage::Response = serde_json::from_slice(&encoded_resp)?;
    let migcred = check_blockage::handle_response(state, decoded_resp)?;
    Ok(migcred)
}

// Migrate to a new bucket (must be level >= 3)
pub async fn blockage_migration(
    net: &dyn Networking,
    lox_cred: &lox_library::cred::Lox,
    migcred: &lox_library::cred::Migration,
    lox_pub: &IssuerPubKey,
    migration_pub: &IssuerPubKey,
) -> Result<lox_library::cred::Lox> {
    let (req, state) = blockage_migration::request(lox_cred, migcred, lox_pub, migration_pub)?;
    let encoded_req: Vec<u8> = serde_json::to_vec(&req)?;
    let encoded_resp = net
        .request("/blockagemigration".to_string(), encoded_req)
        .await?;
    let decoded_resp: blockage_migration::Response = serde_json::from_slice(&encoded_resp)?;
    let cred = blockage_migration::handle_response(state, decoded_resp, lox_pub)?;
    Ok(cred)
}

#[cfg(test)]
mod tests;
